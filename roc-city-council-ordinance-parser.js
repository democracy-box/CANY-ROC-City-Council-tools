const fs = require('fs');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;

const inFiles = fs.readdirSync('./raw/ordinance_txt');
console.log(inFiles);
let csvData = [];

inFiles.forEach((inFilename) => {
	text = fs.readFileSync(`./raw/ordinance_txt/${inFilename}`, 'utf8')
	var ordTexts = text.matchAll(/\nOrdinance No\. (?<ordNum>[0-9-]*)\n(?<ordTitle>.+?)\n(?<ordText>[A-Z ]*?(BE IT|WHEREAS).*?)(Ayes)\s*?-\s*?(?<ordAyeVoters>.+?)\s*?-\s*?(?<ordAyeVoteCount>[0-9]+).*?^(Nays)\s*?-\s*?(?<ordNayVoters>.+?)\s*?-\s*?(?<ordNayVoteCount>[0-9]+)/mgs);

	const ordTextsArray = [...ordTexts];
	csvData = csvData.concat(ordTextsArray.map((match) => ({filename: inFilename, ...match.groups})));

})

const csvWriter = createCsvWriter({
	  path: './data/roc_cc_ordinances.csv',
	  header: [
		      {id: 'filename', title: 'Datafile'},
		      {id: 'ordNum', title: 'Ord. Number'},
		      {id: 'ordTitle', title: 'Title'},
		      {id: 'ordText', title: 'Text'},
		      {id: 'ordAyeVoters', title: 'Aye Voters'},
		      {id: 'ordAyeVoteCount', title: 'Aye Vote Count'},
		      {id: 'ordNayVoters', title: 'Nay Voters'},
		      {id: 'ordNayVoteCount', title: 'Nay Vote Count'},
		    ]
});

csvWriter
  .writeRecords(csvData)
  .then(()=> console.log('The CSV file was written successfully'));

